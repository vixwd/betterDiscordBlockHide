# BetterDiscordBlockHide
  
Hides blocked messages and the message notifying there are blocked messages for Discord with use with Better Discord. 

This is the perfect code for those who wish that the messages of those you dislike or do not wish to engage with are completely taken out of sight - out of mind. 
  
1. Download [Better Discord](https://github.com/BetterDiscord/BetterDiscord)  
2. Use their [Installation](https://docs.betterdiscord.app/users/guides/installing-addons/) Instructions.  
 a. Download any desired [Plugsins](https://betterdiscord.app/plugins)  
 b. Download any desired [Themes](https://betterdiscord.app/themes)  
3. Go to Discord User Settings located at the bottom left of the application. ([See Image](https://imgbox.com/wmJTGUea))  
4. Go to the bottom of the list on the right side of the application, and click on Custom CSS ([See Image](https://imgbox.com/5o7WG4rA))  
5. Copy and Paste  the content of CustomCSS.css and paste  them into the CUSTOM CSS EDITOR] ([See Image](https://imgbox.com/5PVvXkWI))  
6. Save the settings, by clicking the Save Botton ([See Image](https://imgbox.com/my3Cu1cc))  
7. Close setting and Enjoy!
